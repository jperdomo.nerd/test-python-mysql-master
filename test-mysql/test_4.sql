# Generar scripts que realicen la siguiente inserción
# -- Me queda la duda de si era un stored procedure lo que se requeria - Julián Perdomo

-------* Cree una función que permita insertar o crear 5 colores *-------
DELIMITER //

CREATE FUNCTION insertar_colores (color1 VARCHAR(100), 
                                color2 VARCHAR(100), 
                                color3 VARCHAR(100), 
                                color4 VARCHAR(100),
                                color5 VARCHAR(100))
RETURNS INT
BEGIN
    SET @ultimo_id = 0;
    SELECT id INTO @ultimo_id
    FROM colors
    ORDER BY id DESC LIMIT 1;

    SET @ultimo_id = @ultimo_id + 1;
    INSERT INTO colors(code, name) VALUES (@ultimo_id, color1);
    
    SET @ultimo_id = @ultimo_id + 1;
    INSERT INTO colors(code, name) VALUES (@ultimo_id, color2);
    
    SET @ultimo_id = @ultimo_id + 1;
    INSERT INTO colors(code, name) VALUES (@ultimo_id, color3);
    
    SET @ultimo_id = @ultimo_id + 1;
    INSERT INTO colors(code, name) VALUES (@ultimo_id, color4);

    SET @ultimo_id = @ultimo_id + 1;
    INSERT INTO colors(code, name) VALUES (@ultimo_id, color5);

    RETURN 1;
END; //

DELIMITER ;
