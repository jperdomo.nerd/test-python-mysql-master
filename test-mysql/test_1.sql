# Generar scripts que realicen las siguientes consultas

-------* Consultar los items que pertenezcan a la compañia (utilizando INNER JOIN) con ID #3 *-------
SELECT i.id,
    i.name,
    i.price,
    i.cost
FROM companies AS c
WHERE id = 3
INNER JOIN items AS i ON c.id = i.companyId;

-------* Mostrar los últimos 10 items *-------
SELECT *
FROM items
ORDER BY id DESC LIMIT 10;

-------* Mostrar los items que en el nombre terminen con la letra A *-------
SELECT *
FROM items
WHERE name LIKE '%a';

-------* Mostrar los items que tengan relacionado el color Rojo *-------
SELECT *
FROM items
WHERE colorId = (SELECT id 
                FROM colors
                WHERE name = 'ROJO');
